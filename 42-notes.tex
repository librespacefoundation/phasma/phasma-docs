\documentclass{article}
\usepackage{graphicx} % Required for inserting images
\usepackage{hyperref}
\usepackage{xcolor, soul}
\usepackage{bm}
\usepackage{amsmath}

\newcommand\TBD[1]{\textcolor{purple}{#1}}

\title{Notes on using the 42 simulator}
\author{PHASMA Mission -- ADCS Team}
\date{\today}

\begin{document}

\maketitle

\section{Setting a SC 3D model}
\begin{itemize}
    \item One must put the .obj file in the \texttt{42\_install\_dir/Model} directory.
    \item One must change the line \texttt{mtllib} to \texttt{42.mtl} (this line is found at the beginning of the file).
    \item Take into account that information regarding both the definition of the axes and the origin in the CAD system are included in the obj file. This raises 2 points of attention:
    \begin{itemize}
        \item Make sure that the axes in the CAD model are aligned with the nominal attitude.
        \item Make sure to measure the position of the center of mass from the origin of the CAD file. In theory, this would not necessitate attention, but since it is common for a simplified model of the SC to be designed, it is possible that the origin in the simplified model is chosen to be different than the one on the hi-fi model (from which the position of the center of mass is measured).
    \end{itemize}
    \item Depending on the CAD program where the model was created, the .obj file might have to be scaled. The most probable scaling scenarios are from mm to m and from in to m, and a scaling code in Python can be found \href{https://blender.stackexchange.com/questions/114010/scaling-directly-in-obj-file}{here}. Whether scaling is needed or not can be determined by one or a combination of the following means:
    \begin{itemize}
        \item Open an .obj file from the provided ones for a spacecraft of similar geometry
        \item Open the visualizer for a spacecraft of similar geometry and check the value needed for the \texttt{range} parameter in order for it to be visible. Then check the \texttt{range} parameter needed for the model under investigation to determine whether it must be scaled up/down.
    \end{itemize}
\end{itemize}

\section{Initial conditions definition}
The first parameter to set is on line 13 of the SC*.txt file. This line is a combination of 3 letters defining the following:
\begin{itemize}
    \item The first letter denotes the frame on which the angular velocity is referenced and can either be \texttt{N} for the N-frame or \texttt{L} for the L-frame.
    \item The second letter denotes whether the initial attitude will be represented by quaternions or Euler angles and can either be \texttt{Q} or \texttt{A} respectively.
    \item The third letter denotes the frame on which the initial attitude is referenced and can either be \texttt{N} for the N-frame, \texttt{L} for the L-frame, or \texttt{F} for the F-frame.
\end{itemize}

\section{Inner workings of 42}
Since the inner operational schemes of the simulator are not always fully documented, some remarks are listed here.
These notes are not necessarily aimed towards programmers, so they might contain information that seem trivial or obvious to them.
\subsection{Inherent "static" variables}
Sometimes, control laws and estimators need access to the past value of some variables, so they shall not be cleared in every time step of the simulation.
Of course, one might re-declare the required variables in their functions as static, but this is cumbersome, so it helps to know which of the variables provided by the program do not change in every time step.
The variables of this type that have been identified so far are listed below (of course one can look into the code for further exploration):
\begin{itemize}
    \item Commanded torques, \texttt{Tcmd[]}, found in the \texttt{ struct xCtrlType} member of the \texttt{struct AcType AC} member of a \texttt{struct SCType} struct.
\end{itemize}

\subsection{FSW Timestep for \texttt{PROTOTYPE\_FSW}}
It was empirically determined that a large FSW sampling time can result in instability when using \texttt{PROTOTYPE\_FSW}.
This makes logical sense since the ideal actuators used in this flight software mode might be commanded to prodece very large forces/torques to counter the large error accumulated during the large sampling time.
Indicatively, for a simulation time step of 1 sec, a FSW timestep of 15 sec resulted in instability, while a FSW timestep of 1 s was able to maintain the commanded attitude. Simulation was performed on a 3U Cubesat in SOO with 550 km altitude and all torque disturbances turned off.

The fact that an instability occurred with disturbances turned off is a bit worrying nevertheless.
Maybe it was caused because the commanded attitude was to follow the L frame and the J2 perturbation was enabled, so the L frame was not constant and some indirect for of attitude change command was present.
The issue was not investigated further.

\subsection{Initial command in \texttt{InpCmd.txt}}
It was painfully discovered that for flight softwares utilizing the \texttt{ThreeAxisAttitudeCommand} function, an initial attitude command must be given in \texttt{InpCmd.txt}.
For example, the following code block will return a \texttt{Bogus Euler Sequence 0 in A2C} error:
\begin{verbatim}
    0.0 SC[0] FswTag = PROTOTYPE_FSW
    50.0 SC[0] Cmd Angles = [10.0 50.0 0.0] deg, Seq = 123 wrt L Frame
\end{verbatim}
while the following code block will run fine:
\begin{verbatim}
    0.0 SC[0] FswTag = PROTOTYPE_FSW
    0.0 SC[0] Cmd Angles = [0.0 0.0 0.0] deg, Seq = 123 wrt L Frame
    50.0 SC[0] Cmd Angles = [10.0 50.0 0.0] deg, Seq = 123 wrt L Frame
\end{verbatim}

\subsection{Always \texttt{make clean} before \texttt{make}}
Simple as that.
Just do it.

\subsection{Use small configuration directory names}
There seems to be a limit on the maximum name of either the directory path or the directory name where the simulation configuration files are saved.
Most probably it is the path since 42 does not know the directory name by itself.
What is worse is that the returned error is unrelated.
For example, in one case it was:

\texttt{Error opening 42.mtl: No such file or directory}.

\subsection{Initialization quirks}
There are some eccentric behaviors in the way 42 initializes things.
Starting with a general instruction from 42's documentation, users must be extra careful not to modify the structure of the input files because this might cause problems.
Additional to that comment, some known weird behaviors have been observed during our experience and those are listed below:
\begin{itemize}
    \item Do not initialize with quaternions wrt the L frame.
    When initializing the simulation with quaternions wrt the L frame and all disturbances off, the satellite showed a drift over time for no apparent reason.
    This happened for quaternions other than \{0.0, 0.0, 0.0, 1.0\}, normalized up to their 4th decimal.
    When the same simulation was run using any Euler angles wrt the L frame as initialization, there was no drift.
\end{itemize}

\section{Conventions - Notation}
\subsection{Quaternion scalar convention}
The simulator uses the scalar last convention for the quaternion representation.

\subsection{Quaternion multiplication}
When multiplying quaternions, there are 2 possible operations, the $\otimes$ and the $\odot$.
To define these operators, consider two quaternions, $A = [A_0, A_1, A_2, A_3]$ and $B = B_0, B_1, B_2, B_3$, using the scalar--last convention.
The $\otimes$ multiplication is defined as :
$$
\bm{A} \otimes \bm{B} = 
\begin{bmatrix} 
    B_3 \bm{A_{0:2}} + A_3 \bm{B_{0:2}} -\bm{A_{0:2}} \times \bm{B_{0:2}}\\
    A_3 B_3 - \bm{A_{0:2}} \cdot \bm{B_{0:2}}
\end{bmatrix}
$$
The $\odot$ multiplication is very similar to the $\otimes$, with the only difference being the sign of the cross product :
$$
\bm{A} \odot \bm{B} = 
\begin{bmatrix} 
    B_3 \bm{A_{0:2}} + A_3 \bm{B_{0:2}} +\bm{A_{0:2}} \times \bm{B_{0:2}}\\
    A_3 B_3 - \bm{A_{0:2}} \cdot \bm{B_{0:2}}
\end{bmatrix}
$$
The two multiplications are related via the following property :
$$
\bm{A} \otimes \bm{B} = \bm{B} \odot \bm{A}
$$
The multiplication used by 42 is the $\otimes$ multiplication.
Moreover, in its functions 42 mentions the "complement" of a quaternion.
Since this is not a very widespread nomenclature, we note here that the "complement" refers to the conjugate quaternion, i.e. a quaternion with the same magnitude but different signs on the imaginary part.

\subsection{Angular velocity frame}
In file \texttt{Database/42.json} it is written that \texttt{wn} denotes "The angular velocity of B wrt N expressed in B".
This is also verified in \texttt{Include/42types.h} in struct \texttt{BodyType} in variable \texttt{wn}.
An ambiguity arises due to the fact that the angular velocity used in the control laws is denoted with \texttt{AC->wbn} (2 indices instead of only one), but the ambiguity is cleared due to the fact that in \texttt{Source/42sensors.c} in \texttt{void Sensors} in the gyro update part, the following assignment is made \texttt{AC->wbn[i] = S->B[0].wn[i]}.
\TBD{If wbn is expressed in B, then wrn should be expressed in R. However, in many functions 42 calculates werr as wbn--wrn, so they must be in the same frame. I suspect this frame is the N frame}.


\subsection{The xvy notation}
In many output files, one can find the \texttt{xvy} notation, where \texttt{x} and \texttt{y} are different, or the same, letter. In this notation, \texttt{x} denotes the vector that is represented and \texttt{y} denotes the frame on which this vector is represented. Some examples:
\begin{itemize}
    \item \texttt{x=s} for the sun vector.
    \item \texttt{x=b} for the magnetic field.
    \item \texttt{y=b} for the body frame.
    \item \texttt{y=n} for the N frame.
\end{itemize}

\section{Inter Process Communication (IPC) socket}
Conveniently enough, 42 provides the option to write to and read from a socket.
Besides providing output data for logging or live plotting, most crucially, this capability can serve the purpose of abstracting the estimation and control logic from 42 and using it only as a high-fidelity dynamics engine.
Admittedly programming a control law in 42 requires a fair bit of trouble, with even an adjustment to the gains requiring to recompile the program, not to mention messing with the notation and variable naming, which can be frustrating.
The use of the IPC socket in ACS mode is examined below.
In this mode, 42 outputs sensor (and truth) information and receives actuator commands.

\subsection{Choosing output message data}
The output data packet is configurable and the user can select which information are transferred through the socket.
In order to configure the output message, 42 uses 2 python scripts : a script that generates a JSON configuration file and another script that creates C code from that JSON file.
In order to choose which information fields will be transfered, the user has to markup the header files \texttt{42types.h} and/or \texttt{AcTypes.h}.
Both scripts along with documentation on the aforementioned markup can be found in \texttt{42/Database}.

An important point is that the prefixes in the \texttt{Inp\_IPC.txt} file must be set correctly in order for the user to receive the data they anticipate.
For example, if one wants to receive the current magnetic dipole moment of the magnetorquers, this is not available as an internal variable on the \texttt{AcMtbType} struct in \texttt{AcTypes.h} (well, the commanded magnetic dipole moment is available, but for the sake of the example let's ignore it).
The user can receive the current magnetic dipole moment from the \texttt{MTBType} struct if they append the appropriate markup next to the variable.
In this case, the user should use just \texttt{"SC[x]"} as a prefix in order not to constrain the received information to include only variables from the structs in \texttt{AcTypes.h} (which would be the case if the prefix \texttt{"SC[x].AC"} had been used).

As a final note, without having exhausted the sensor and actuator options offered by 42 and considering that the commanded actuation actions are the ones realized by the actuators (see section \ref{sec:using_actuators}), we would claim that the structures in \texttt{AcTypes.h} contain all the information one might need.

\section{Output files}
Besides the IPC capabilities, 42 also provides some default output files.
The contents of some of them are documented below.

\subsection{Attitude information}
Attitude information is provided by 42 either in the form of quaternions or in the form of Euler angles in the 2 following files (csv format with a single space delimiter) :
\begin{itemize}
    \item \texttt{qbn.42} : Provides the quaternions in a scalar-last convention format(the scalar part of the quaternion is the last collumn). Those quaternions represent the rotation from the ECI frame (N frame) to the body frame.
    \item  \texttt{RPY.42} : Provides the SC's Euler angles in the LVLH frame (L frame) The "123" or "Roll Pitch Yaw" or "XYZ" (the three names are equivalent) sequence is followed.
\end{itemize}

\subsection{Actuator information}
So far, only reaction wheels and magnetorquers have been used as actuators.
A report regarding the angular momentum of the wheels and the magnetic dipole moment of the magnetorquers.
Getting the report for those is pretty straightforward : just look into the \texttt{Hwhl.42} and \texttt{MTB.42} files for the reaction wheels and the magnetorquers respectively.
Something to note when writing the post-processing functions, is that (in contrast with the \texttt{RPY.42} file) the actuator output files have a white space character before the new line character.


\subsection{Environment torques output}
The simulator outputs the total environment torques and the total angular momentum of the spacecraft in the body frame and in a "special Sun-orbit" frame to the \texttt{EnvTrq00.42} file. The columns of the file are : 

$Hx_S,Hy_S,Hz_S,Tx_S,Ty_S,Tz_S,Hx_B,Hy_B,Hz_B,Tx_B,Ty_B,Tz_B$,

where the subscript \texttt{S} denotes the special Sun-orbit frame, the subscript \texttt{B} denotes the body frame, \texttt{$H_{k}$} denotes the total angular momentum of the spacecraft in an axis \texttt{k}, and \texttt{$T_{k}$} denotes the total environment torques acting on the spacecraft.

\subsection{Magnetometer and gyro readings output}
To enable output of magnetometer and gyro readings in a report file, uncomment the respective lines found at the end of the \texttt{void Report} function in the \texttt{Source/42report.c} file. Details on how these files are created and what is the meaning of each column can be found in the \texttt{Source/42report.c} file under the respective function (for example, \texttt{void MagReport} for the magnetometer).

\section{Setting up a controller}
\subsection{Receiving commands}
When writing the controller inside 42 (i.e. not using the ACS mode of the IPC socket), one can use the \texttt{ThreeAxisAttitudeCommand(S)} function, which essentially reads the attitude command from the \texttt{Inp\_Cmd.txt} file and transfers it into a \texttt{CMDType} struct.

\subsection{Using actuators}\label{sec:using_actuators}
\begin{enumerate}
    \item To use actuators in a controller, the user must apply the saturation in the controller function because 42 simply sets the actuator action equal to the commanded one.
The \texttt{Limit()} function offered by 42 is useful for that purpose.
    \item When using custom actuators, setting the \texttt{AC->IdealTrq} (and similarly the ideal force) does not have any effect.
\end{enumerate}

\subsection{Using sensors}\label{sec:using_sensors}
\begin{enumerate}
    \item As is the case with actuators, when using custom sensors, the respective ideal vectors have no meaning and are constantly zero.
    For example \texttt{AC->bvb} will be constantly zero when magnetometers are used.
    However, the fields of the \texttt{SCType} struct are correctly updated.
    This means that when a magnetometer is used, but one wants to read the true magnetic field values, they must use \texttt{SC->bvb} and not \texttt{AC->bvb}.

    \item When using a Fine Sun Sensor, the following are helpful:
    \begin{itemize}
        \item If a pointer to an \texttt{struct AcType AC} is available, then the sensor is referred to as \texttt{AC->FSS}.
        \item From the fields of the \texttt{struct AcFssType}, \texttt{double SunVecS[3]} is the sun vector in the sensor frame and \texttt{double SunVecB[3]} is the sun vector in the SC body frame.
        \item Since FSS readings are not always available, it is a good idea to add a check of the \texttt{long Valid} field of the \texttt{struct AcFssType}.
    \end{itemize}

    \item Sensor readings need processing.
    For example, just reading the \texttt{SunVecB} array member of a  \texttt{struct AcFssType} will not yield useful results.
    Some "sometimes naive" (as characterized by the creator of 42 himself) processing functions are provided in the \texttt{AcApp.c} file.
\end{enumerate}


\end{document}
